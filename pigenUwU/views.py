from django.shortcuts import render
from datetime import datetime

# Create your views here.
waktu = datetime.now()

def home(request):
    return render(request,'pages/Story3.html',{'time':waktu})

def about(request):
    return render(request,'pages/Story3.html',{'time':waktu})

def more(request):
    return render(request,'pages/baru.html',{'time':waktu})

def signup(request):
    return render(request,'pages/sign-up.html',{'time':waktu})
